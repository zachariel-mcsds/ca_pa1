import matplotlib.pyplot as plt

import math
import random
import numpy as np

def file_content(filename='data/places.txt'):
    """Read file and yiel line by line"""

    with open(filename, 'r') as f:
        for line in f:
            x, y = line.strip().split(',')
            yield [float(x), float(y)]

def euclidean_distance(p1, p2):
    """
    Euclidean distance:
        sqrt( pow(p1[0] - p2[0], 2) + pow(p1[1] - p1[1], 2) )
    """
    return math.sqrt( math.pow(float(p1[0]) - float(p2[0]), 2) + math.pow(float(p1[1]) - float(p2[1]), 2) )

class Document:
    def __init__(self, id, data):
        self.id = id
        self.data = data

class Cluster:
    def __init__(self, centroid=None):
        self.centroid = centroid
        self.documents = list()
        self.elements = np.empty(shape=(0,2))#array([[]]) #dtype((np.float, (2, 1)))

    def x(self):
      return self.elements[:, 0]

    def y(self):
      return self.elements[:, 1]

    def __str__(self):
      return str(self.ids())

    def __eq__(self, other):
      return self.documents == other.documents

    def addElement( self, element ):
        self.documents.append(element)
        self.elements = np.append( self.elements, [element.data], axis=0 )
        self.mean()

    def calculateDistance(self, document):
        return euclidean_distance(self.centroid, document)

    def mean( self ):
        if len(self.elements) > 1:
            self.centroid = self.elements.mean(axis=0)

    def size( self ):
        return len(self.elements)

    def ids(self):
        ids = [document.id for document in self.documents]
        ids.sort()
        return ids

class KMeans:
    def __init__(self, k=3, documents=[]):
        self.documents = documents
        self.k = k
        self.last_clusters = [] #Cluster() for i in range(0, k)]
        self.clusters = [] #Cluster() for i in range(0, k)]

    def __str__(self):
      return str(self.__dict__)

    def __eq__(self, other):
      return [cluster.ids() for cluster in self.last_clusters] == [cluster.ids() for cluster in self.clusters]

    def addDocument( self, document ):
        self.documents.append( document )

    def run( self ):
        self.init_centroids()
        runs = 0
        while runs <= 1000:
            for document in self.documents:
                self.getClosest(document).addElement( document )

            #if len(self.clusters) > 0:
            #  if len(self.last_clusters) == 0:
            #    self.resetClusters()
            #  else:
            #    if self.last_clusters == self.clusters:
            #      print('%s no changes' % runs)
            #      break
            #    else:
            #      self.resetClusters()

            self.resetClusters()

            runs += 1

        self.clusters = self.last_clusters

    def getClosest(self, document):
        closestCluster = None
        closest = None
        for cluster in self.clusters:
            distance = euclidean_distance(document.data, cluster.centroid)
            if not closest:
                closest = distance
                closestCluster = cluster

            if distance < closest:
                closest = distance
                closestCluster = cluster

        return closestCluster

    def resetClusters( self ):
        self.last_clusters = self.clusters
        self.clusters = [Cluster(cluster.centroid) for cluster in self.clusters]

    def init_centroids( self ):
        random.shuffle( self.documents )
        self.clusters = [Cluster(document.data)  for document in self.documents[:self.k]]

    def centroids(self):
        return [cluster.centroid for cluster in self.clusters]


    def write_to_file(self, output='data/output.txt'):
      with open(output, 'w') as f:
        for cidx, cluster in enumerate(self.clusters):
            for document in cluster.documents:
              f.write("%s %s\n" % (document.id, cidx))



def main():
  #kmeans = KMeans(k=2)

  #for idx, document in enumerate(file_content('data/sample.txt')):
  #    kmeans.addDocument( Document(idx, document) )

  kmeans = KMeans()

  fig = plt.figure()
  ax = fig.add_subplot(111)

  points = np.empty(shape=(0,2))
  for idx, values in enumerate(file_content()):
      kmeans.addDocument( Document(idx, values) )
      points = np.append(points, [values], axis=0)

  ax.scatter(points[:,0], points[:,1])

  kmeans.run()
  kmeans.write_to_file('data/output.txt')

  fig2 = plt.figure()
  ax2 = fig2.add_subplot(111)

  cluster_points = np.empty(shape=(0,2))
  for cidx, cluster in enumerate(kmeans.clusters):
      ax2.plot(cluster.x(), cluster.y(), marker='o', linestyle='', ms=12, label=cidx)
      #cluster_points = np.append(cluster_points, cluster.elements, axis=0)
  #ax2.scatter(x, y)
  #ax2.scatter(cluster_points[:,0], cluster_points[:,1])
  ax2.legend()

  plt.show()

  #for cidx, cluster in enumerate(kmeans.clusters):
  #    for document in cluster.documents:
  #      print("%s %s" % (document.id, cidx))

if __name__ == "__main__":
  main()
